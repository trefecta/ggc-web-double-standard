#!/usr/bin/env python

"""
    Basic Static Server for Development.

        NOTE: `$>` means command line (aka CLI)

    1. Double click or `$> ./server.py`
    2. Navigate to browser.
    3. Go to 'http://localhost:8080'
    4. To Exit, close terminal window OR `$> {{CTRL+C}}`

"""

import SimpleHTTPServer
import SocketServer

HOST = 'localhost'
PORT = 8080
ADDRESS = (HOST, PORT)

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

server = SocketServer.TCPServer(ADDRESS, Handler)

server.serve_forever()