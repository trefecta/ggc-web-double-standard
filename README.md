Website for *Double Standard* Bar
=================================

Website made by Dante Rouser for Double Standard Bar

1. Clone git repository locally:
    git clone git clone git@bitbucket.org:trefecta/ggc-web-double-standard.git

2. Use Git to [review the old commits](https://www.atlassian.com/git/tutorials/viewing-old-commits/) and view the [diffs](https://git-scm.com/docs/git-diff) between them.

3. Use `server.py` to run locally
  * From the file graphical user interface (GUI) (aka Finder/Explorer): double-click the file
  * From command line / terminal: `./server.py`
  * NOTE: This file may require setting the permissions of the file to 'executable'.
